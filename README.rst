Overview
========

parched aims at providing a python module capable of parsing pacman
packages and PKGBUILDs.

Current state
=============

I’m current moving things around, so use by other people isn’t really
recommended.

Documentation
=============

The documentation *is* available online; see here:

http://rls.bitbucket.org/parched/


License
=======

parched is MIT licensed. Do whatever you want.
