# -*- coding: utf-8 -*-
#
# parched documentation build configuration file, created by
# sphinx-quickstart on Mon Feb  2 15:33:13 2009.
#
# This file is execfile()d with the current directory set to its containing dir.
#
# The contents of this file are pickled, so don't put values in the namespace
# that aren't pickleable (module imports are okay, they're removed automatically).

import sys, os

sys.path.append(os.path.abspath('../'))

# General configuration
# ---------------------

extensions = ['sphinx.ext.autodoc', 'sphinx.ext.intersphinx']
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
project = u'parched'
copyright = u'2009, Sebastian Nowicki; 2013, Neil Santos'
version = '0.2'
release = '0.2.0-beta'
exclude_trees = ['_build']
pygments_style = 'tango'
intersphinx_mapping = {
    'python': ('http://docs.python.org/2', None),
    'python3': ('http://docs.python.org/3', None)
}

autoclass_content = "both"
autodoc_member_order = "bysource"


# Options for HTML output
# -----------------------

html_theme = 'pyramid'
html_use_modindex = False
html_use_index = False
html_copy_source = False
html_show_source_link = False
