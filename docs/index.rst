.. toctree::
   :hidden:

############################################
:mod:`parched` --- Parse Arch Linux packages
############################################

.. automodule:: parched
   :synopsis: Parse Arch Linux's pacman packages and PKGBUILD recipes.

This module defines three classes which provide information about Pacman
packages and PKGBUILDs, :class:`Pacman`, :class:`Pkgbuild`, and
:class:`LocalPackage`.  These classes inherit from the :class:`Package`
class, which provides the basic metadata about package.

**********************
Basic package metadata
**********************

The :class:`Package` contains attributes common to all instances of
:class:`Pacman`, :class:`Pkgbuild`, and :class:`LocalPackage`.  It
should be considered an abstract class, not to be used directly.

.. autoclass:: Package
    :members:

.. autofunction:: Checksum

**********************************************
Retrieving information from `pacman`_ packages
**********************************************

The :class:`Pacman` class provides information about a binary package,
by parsing a tarball in `pacman`_ package format.  Specifically, it
looks for a file named ``.PKGINFO`` inside the tarball, which is then
parsed to extract the target package's metadata.

.. note::

    Plain and compressed tar archives are supported---which compression
    algorithms are supported depend on the version of Python.

    When on Python 2.x, only :mod:`gzip <python:gzip>` and :mod:`bzip2
    <python:bz2>` compression is supported---on Python 3.x, the list of
    compressions supported include :mod:`lzma <python3:lzma>`.  There
    are, however, plans to eventually support all the other compression
    mechanisms supported by :manpage:`MAKEPKG(8)`, regardless of the
    Python version used.

.. autoclass:: Pacman(target = <path-or-fileobj>)
   :members:

.. seealso:: :class:`Package`, :class:`Pkgbuild`, :class:`LocalPackage`, :class:`AURPackage`

***********************************************
Retrieving information from `PKGBUILD`_ recipes
***********************************************

The :class:`Pkgbuild` class provides information about a package by
parsing a :manpage:`PKGBUILD(5)` file.

.. note::

    `PKGBUILD`_ recipes are basically `bash`_ scripts, and there are
    specimens that take full advantage of this.  As this class doesn’t
    (and likely will never) feature a full-blown `bash`_ lexer and
    parser, expect some troubles with the more... *esoteric* recipes.

    This library’s original author has written a minimal `PKGBUILD`_
    parser using Lex/Yacc (and branched off this library to take
    advantage of it).  However, as it’s been about four years since
    `pkgparse`_ has seen any activity.  That, and the fact that this
    library does most of what I need it to (so far), means it’ll be a
    while before things change.

.. autoclass:: Pkgbuild(target = <path-or-fileobj>)
   :members:

.. seealso:: :class:`Package`, :class:`Pacman`, :class:`LocalPackage`, :obj:`Checksum`, :class:`AURPackage`

**********************************************
Retrieving information from installed packages
**********************************************

The :class:`LocalPackage` class parses the ``desc`` file `pacman`_ keeps
for a locally-installed package.  As with the other classes from this
library, only access to the file to be read is required; this class can
of course be used regardless of the underlying operating system.

.. note::

   With Arch being a rolling-release distro (and me being reasonably
   fastidious about keeping my systems up-to-date), this class will only
   ever support the latest format of ``desc`` meta files (although there
   will likely be some lag if/when it changes).

.. autoclass:: LocalPackage(target = <path-or-fileobj>)
   :members:

.. seealso:: :class:`Package`, :class:`Pacman`, :class:`Pkgbuild`, :class:`AURPackage`

****************************************
Retrieving information from AUR packages
****************************************

The :class:`AURPackage` class is a wrapper for the package data returned
by a query to the `AUR`_ server.  This is a convenience class, so AUR
query data can be manipulated much like any other `pacman`_ package
meta.

.. autoclass:: AURPackage(target = <dict-from-JSON>)
   :members:

.. seealso:: :class:`Package`, :class:`Pacman`, :class:`Pkgbuild`, :class:`LocalPackage`


.. _AUR: https://aur.archlinux.org/
.. _bash: https://gnu.org/software/bash/
.. _pacman: https://www.archlinux.org/pacman
.. _PKGBUILD: https://wiki.archlinux.org/index.php/PKGBUILD
.. _PKGINFO: https://wiki.archlinux.org/index.php/Creating_Packages#Overview
.. _pkgparse: https://github.com/sebnow/pkgparse/
