# -*- coding: utf-8 -*-
# Copyright (c) 2013 Neil Santos
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

""".. currentmodule:: parched"""

from .package import Package


class AURPackage(Package):
    """
    Parse package data returned by a query to the `AUR`_ server.

    After instantiation, the package's metadata can then be accessed
    directly.

    *target* must be a dict; AURPackage will not attempt to convert JSON
    data into a Python type on its own.

        >>> aur_data = json.loads(json_from_aur_query)
        >>> pkg = AURPackage(aur_data)
        >>> print(pkg)
        "otf-texgyre 2.005-2"
        >>> print(pkg.url)
        "http://www.gust.org.pl/projects/e-foundry/tex-gyre"

    Note that :class:`AURPackage` does not map all AUR return values,
    only those they have in common with the other
    :class:`Package`-derived classes.
    """
    def __init__(self, dict_info):
        super(AURPackage, self).__init__()
        target_keys = ["Name", "Version", "Description", "URL", "License"]

        [setattr(self, key.lower(), dict_info[key])
         for key in target_keys if key in dict_info]

        if self.version:
            self.version, _, self.release = self.version.rpartition("-")
