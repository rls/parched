# -*- coding: utf-8 -*-
# Copyright (c) 2013 Neil Santos
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

""".. currentmodule:: parched"""

from __future__ import print_function

from .pacman import Pacman
from datetime import datetime
import os.path


class LocalPackage(Pacman):
    """
    Parse `pacman`_ metadata on installed packages.

    After instantiation, the package's metadata can then be accessed
    directly.

    If *target* is a string, the string is interpreted as the path to a
    `pacman`_ package::

        >>> installed_pkg = LocalPackage("/var/lib/pacman/local/libao-1.1.0-3/desc")
        >>> print(installed_pkg)
        "libao-1.1.0-3"
        >>> print(installed_pkg.description)
        "Cross-platform audio output library and plugins"

    Otherwise, it must be a readable :ref:`file-like object
    <python:bltin-file-objects>`::

        >>> fd_desc = open("/var/lib/pacman/local/ppp-2.4.5-8/desc")
        >>> pkg_fd = LocalPackage(fd_desc)
        >>> print(pkg_fd)
        "ppp-2.4.5-8"
        >>> print(pkg_fd.description)
        "A daemon which implements the Point-to-Point Protocol for dial-up networking"
        >>> fd_desc.close()

    .. note::

        In the latter case, the passed-in file handle is *not* automatically closed.

    In addition to the attributes provided by :class:`Package`, and
    :class:`Pacman`, :class:`LocalPackage` provides the following attributes:
    """
    def __init__(self, target):
        try:
            self._symbol_map.update({
                "desc": "description",
                "installdate": "install_date",
            })
            self.field_lists_map.append("depends")
            #: A :class:`datetime <python:datetime.datetime>` object
            #: indicating when the package was installed.
            self.install_date = 0
            super(LocalPackage, self).__init__(target)
        except (AttributeError, TypeError):
            # We aren't interested in handling tarballs.
            try:
                with open(os.path.abspath(target), mode="rU") as fd:
                    self._parse(fd)
            except (AttributeError, TypeError):
                # target isn't a string; maybe a file?
                self._parse(target)

        if self.build_date:
            self.build_date = datetime.utcfromtimestamp(int(self.build_date))
        if self.install_date:
            self.install_date = datetime.utcfromtimestamp(int(self.install_date))
        if self.size:
            self.size = int(self.size)
        if self.version:
            self.version, _, self.release = self.version.rpartition("-")

    def _parse(self, desc):
        """Parse the desc file"""
        if hasattr(desc, "seek"):
            desc.seek(0)

        marker = None
        for line in desc:
            if line.strip() == "":
                marker = None
                continue

            if line.startswith("%") and line.strip().endswith("%"):
                marker = line.lower().strip().strip("%")
            elif marker is not None:
                self._stash(marker, line.strip())

    def _stash(self, name, value):
        """Handle assignment of values to their respective fields."""
        symbol = self.field_to_attr(name)

        if name in self.field_lists_map:
            field_value = getattr(self, symbol)
            field_value.append(value)
            value = field_value

        setattr(self, symbol, value)
