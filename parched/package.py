# Copyright (c) 2009 Sebastian Nowicki
# Copyright (c) 2013 Neil Santos
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

""".. currentmodule:: parched"""

class Package(object):
    """This class provides no functionality whatsoever.  To actually do
    useful stuff, refer to :class:`Pacman`, :class:`Pkgbuild`, or create
    your own subclass.  It exists mostly to hold the following
    attributes, all of which map to metadata fields common to all
    `pacman`_ packages.

    For more information about these attributes see :manpage:`PKGBUILD(5)`.
    """
    #: Maps `PKGINFO`_ fields to (slightly more Pythonic) attribute
    #: names.
    _symbol_map = {
        "pkgname": "name",
        "pkgver": "version",
        "pkgdesc": "description",
        "pkgrel": "release",
        "arch": "architectures",
        "conflict": "conflicts",
        "depend": "depends",
        "license": "licenses",
        "group": "groups",
        "makedepend": "makedepends",
        "optdepend": "optdepends",
        "makepkgopt": "options"
    }
    #: The .PKGINFO fields/properties that map to lists.
    #:
    #: Both :class:`Pacman: and :class:`Pkgbuild` extend this as
    #: appropriate.
    field_lists_map = [
        "arch",
        "conflict",
        "conflicts",
        "depend",
        "depends",
        "group",
        "groups",
        "license",
        "makedepend",
        "makedepends",
        "makepkgopt",
        "options",
        "optdepend",
        "optdepends",
        "provides",
        "replaces"
    ]

    def __init__(self):
        #: The name of the package.
        self.name = ""
        #: The version of the package, as a string.
        self.version = ""
        #: Release version of the package, i.e., version of the package
        #: itself, as an integer.
        self.release = ""
        #: Description of the package.
        self.description = ""
        #: CPU architectures supported by the package.
        self.architectures = []
        #: Package's website.
        self.url = ""
        #: List of licenses applicable to the package.
        self.licenses = []
        #: List of package groups the package belongs to.
        self.groups = []
        #: List of "virtual provisions" that the package provides.
        self.provides = []
        #: List of other packages the package requires to function.
        self.depends = []
        #: List of packages needed to successfully compile this package.
        self.makedepends = []
        #: List of other packages that provide additional functionality,
        #: but are not required during runtime.
        self.optdepends = []
        #: List of other packages the package conflicts with.
        self.conflicts = []
        #: List of packages this package replaces.
        self.replaces = []
        #: Architectures the package can be installed on.
        self.architectures = []
        #: List of paths that should be backed up on upgrades.
        self.backup = []
        #: Options used when building the package, represented as a
        #: list. This list is equivalent to that of `options` in a
        #: `PKGBUILD`_. See :manpage:`PKGBUILD(5)` for more information.
        self.options = []

    def __contains__(self, key):
        return (key in self._symbol_map.values())

    def __str__(self):
        return "{0} {1}-{2}".format(self.name, self.version, self.release)

    @classmethod
    def field_to_attr(self, field):
        return self._symbol_map.get(field, field)
