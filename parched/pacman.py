# Copyright (c) 2009 Sebastian Nowicki
# Copyright (c) 2013 Neil Santos
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

""".. currentmodule:: parched"""

from .package import Package
from datetime import datetime
import os.path, tarfile

class Pacman(Package):
    """
    Parse a `pacman`_ package and extract its metadata.

    After instantiation, the package's metadata can then be accessed
    directly.

    If *target* is a string, the string is interpreted as the path to a
    `pacman`_ package::

        >>> pkg_tarball = Pacman("/var/cache/pacman/pkg/emacs-bzr-113441-1-i686.pkg.tar.gz")
        >>> print(pkg_tarball)
        "emacs-bzr 113441-1"
        >>> print(pkg_tarball.description)
        "The extensible, customizable, self-documenting real-time display editor from its official Bzr repository"

    Otherwise, it must be a readable :ref:`file-like object
    <python:bltin-file-objects>`::

        >>> fd_tarball = open("/var/cache/pacman/pkg/bzr-dev-6579-1-i686.pkg.tar.gz")
        >>> pkg_fd = Pacman(fd_tarball)
        >>> print(pkg_fd)
        "bzr-dev 6579"
        >>> print(pkg_fd.description)
        "A decentralized revision control system (development series)"
        >>> fd_tarball.close()

    .. note::

        In the latter case, the passed-in file handle is *not* automatically closed.

    In addition to the attributes provided by :class:`Package`,
    :class:`Pacman` provides the following attributes:
    """
    def __init__(self, target):
        self._symbol_map.update({
            "builddate": "build_date",
            "force": "is_forced"
        })
        self.field_lists_map.append("backup")

        #: A :class:`datetime <python:datetime.datetime>` object
        #: indicating when the package was built.
        self.build_date = 0
        #: Indicates whether an upgrade was forced.
        self.is_forced = ""
        #: List of paths of the files contained in the package.
        self.files = []
        #: Disk space used by the package, as installed, in bytes.
        self.size = 0
        #: The person who built the package, usually of the format::
        #:
        #:     First_name Last_name <email@domain.com>
        self.packager = ""

        super(Pacman, self).__init__()

        try:
            tar_file = None
            # This is a special clause for handling the old parched unit
            # tests.  This exists because I want to exercise this class
            # as much as possible.  THIS MUST NEVER BE USED OTHERWISE.
            if hasattr(self, "i_am_testing_and_want_to_exercise_everything"):
                # target is a one-off instance of class TarFileMock, and
                # can be treated as a tarfile.
                tar_file = target
            else:
                try:
                    tar_file = tarfile.open(os.path.abspath(target), mode="r:*")
                except (AttributeError, TypeError):
                    # target isn't a string; maybe a file?
                    tar_file = tarfile.open(fileobj=target, mode="r|*")

            self.files = tar_file.getnames()
            self._parse(tar_file.extractfile(".PKGINFO"))
            tar_file.close()
        except:
            raise TypeError("Don't know how to use {0}".format(target))

    def _parse(self, pkginfo):
        """Parse the .PKGINFO file"""
        if hasattr(pkginfo, "seek"):
            pkginfo.seek(0)

        for line in pkginfo:
            if line[0] == "#" or line.strip() == "":
                continue

            var, _, value = map(lambda x: x.strip(), line.rpartition(" = "))
            real_name = self.field_to_attr(var)
            if var in self.field_lists_map:
                array = getattr(self, real_name, [])
                array.append(value)
            else:
                setattr(self, real_name, value)

        if self.size:
            self.size = int(self.size)
        if not self.is_forced == False:
            self.is_forced = (self.is_forced == "True")
        if self.build_date:
            self.build_date = datetime.utcfromtimestamp(int(self.build_date))
        if self.version:
            self.version, _, self.release = self.version.rpartition("-")
        if self.packager == "Unknown Packager":
            self.packager = None
