# -*- coding: utf-8 -*-
# Copyright (c) 2009 Sebastian Nowicki
# Copyright (c) 2013 Neil Santos
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

""".. currentmodule:: parched"""

from .package import Package
from io import TextIOWrapper
from collections import namedtuple
import os.path, re, shlex

try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO

Checksum = namedtuple("Checksum", ["checksum", "algo"])
"""
:func:`Named tuple <python:collections.namedtuple>` to represent pairs
of checksums and the algorithm used to produce them.  Used internally by
:attr:`Pkgbuild.checksums`.

  .. attribute:: Checksum.checksum

      The checksum value; length will depend on the algorithm used to
      produce it.

  .. attribute:: Checksum.algo

      The algorithm used to produce *checksum*.  For a definitive list
      of valid values, refer to the ``INTEGRITY_CHECK`` option of
      :manpage:`MAKEPKG.CONF(5)`.

  .. seealso:: :manpage:`MAKEPKG.CONF(5)`
"""

class Pkgbuild(Package):
    """
    Parse a :manpage:`PKGBUILD(5)` recipe and extract its metadata.

    After instantiation, the package's metadata can then be accessed
    directly.

    If *target* is a string, the string is interpreted as the path to a
    `PKGBUILD`_ recipe::

        >>> recipe = Pkgbuild("/var/abs/local/aur/firefox-ux/PKGBUILD")
        >>> print(recipe)
        "firefox-ux 25.0a1-3"
        >>> print(recipe.description)
        "Standalone web browser from Mozilla, UX nightly build"

    Otherwise, it must be a readable :ref:`file-like object
    <bltin-file-objects>`::

        >>> fd_recipe = Pkgbuild("/var/abs/local/aur/heroku-toolbelt/PKGBUILD")
        >>> recipe_fd = Pkgbuild(fd_recipe)
        >>> print(recipe_fd)
        "heroku-toolbelt 2.39.1-1"
        >>> print(recipe_fd.description)
        "Everything you need to get started using Heroku"

    .. note::

        In the latter case, the passed in file handle is *not* automatically closed.

    In addition to the attributes provided by :class:`Package`,
    :class:`Pkgbuild` provides the following attributes:
    """

    #: A dictionary to indicate the presence of known PKGBUILD functions.
    #:
    #: The keys are the names of known PKGBUILD functions, while their
    #: values indicate whether they exist in the current recipe.  Note
    #: that only the *presence* of these tokens are indicated, not whether
    #: they would actually run.  Also, actually retrieving the body of the
    #: functions present is left as an exercise for the user.
    func_flags = {
        "pkgver": False,
        "prepare": False,
        "build": False,
        "check": False,
        "package": False
    }

    checksum_fields = (
        'md5sums',
        'sha1sums',
        'sha256sums',
        'sha384sums',
        'sha512sums',
    )

    _symbol_regex = re.compile(r"\$(?P<name>{[\w\d_]+}|[\w\d]+)")

    def __init__(self, target):
        self._symbol_map.update({'source': 'sources'})
        self.field_lists_map.extend([
            "source",
            "noextract"
        ])
        self.field_lists_map.extend(self.checksum_fields)

        #: The filename of the install scriptlet.
        self.install = ""

        #: A dictionary of source checksums keyed by filename.  The
        #: value is a :obj:`named tuple <Checksum>` containing the
        #: checksum and a representation of the algorithm used to
        #: generate it.  There is one entry for each item in
        #: :attr:`sources`. If more than one checksum type is supplied
        #: for a source, the value will be a list of named tuples.
        #:
        #: .. seealso:: :obj:`Checksum`
        self.checksums = {}

        #: List of the URIs of filenames.  Local file paths can be
        #: relative and do not require a protocol prefix.
        self.sources = []

        #: List of files not to be extracted. These files correspond to
        #: the basenames of the URIs in :attr:`sources`
        self.noextract = []

        super(Pkgbuild, self).__init__()
        # Symbol table
        self._symbols = {}

        #: Map of value assignments found in the current recipe
        self._var_map = {}

        try:
            target_fd = open(os.path.abspath(target), mode="r")
        except (AttributeError, TypeError):
            # target isn't a string; maybe already a file
            target_fd = target
        self._parse(target_fd)
        [setattr(self, self.field_to_attr(key), self._interpolate(val))
         for key, val in self._var_map.items() if key not in self.checksum_fields]

        for field in self.checksum_fields:
            field_values = self._var_map.get(field, None)
            if field_values is None:
                continue
            for idx, source in enumerate(self.sources):
                csum_value = field_values[idx]
                if csum_value.strip().upper() == "SKIP":
                    continue
                csum = Checksum(checksum=csum_value,
                                algo=field[0:field.rfind("sums")])
                if source not in self.checksums.keys():
                    self.checksums[source] = []
                self.checksums[source].append(csum)

    def _interpolate(self, token_value):
        """Perform bash-style interpolation on strings.

        At this point, this function is *severely* retarded.  It will
        not do a lot of the (insane) magic that bash does when
        interpolating strings.  In fact, it does the *bare* minimum;
        just enough for maybe 95% of the PKGBUILD recipes out in the
        wild.
        """
        try:
            if token_value.find("$") == -1:
                return token_value

            lexer = shlex.shlex(StringIO(token_value), posix=True)
            lexer.whitespace = ""
            lexer.whitespace_split = False

            retval = ""
            in_var = False
            while True:
                token = lexer.get_token()
                if token is None:
                    break

                if token == "$":
                    in_var = True
                    continue

                if in_var:
                    if token == "{":
                        continue

                    retval += self._var_map.get(token, "")
                    in_var = False

                    next_token = lexer.get_token()
                    if next_token != "}":
                        lexer.push_token(next_token)
                else:
                    retval += token

            return retval
        except (TypeError, AttributeError):
            return list(map(self._interpolate, token_value))

    def _parse(self, fileobj):
        """Parse PKGBUILD recipe.

        Note that while all working PKGBUILDs are valid bash scripts as
        well, the purpose of this function is to parse only the former.
        That is, this will (and should) never be a full-blown bash
        parser/lexer.  So expect that this will fail when encountering
        some construct that's perfectly valid in bash scripts.

        That said, if this fails to parse a non-trivial, but also not
        too-complicated, PKGBUILD, I’d like to have a copy so I can try
        to make it work.
        """
        if hasattr(fileobj, "seek"):
            fileobj.seek(0)

        lexer = shlex.shlex(fileobj, posix=True)
        lexer.whitespace_split = True

        in_function = False

        brace_guard = 0
        while True:
            token = lexer.get_token()
            if token is None:
                break

            bare_token = token.rstrip("()").strip()
            if bare_token in self.func_flags.keys():
                self.func_flags[bare_token] = True

            if token.endswith("()"):
                token = lexer.get_token()
                if token == "{":
                    brace_guard += 1
                in_function = True

            # We’re not interested in the contents of functions.
            if in_function:
                if token == "}":
                    brace_guard -= 1
                if brace_guard == 0:
                    in_function = False
                continue

            if "=" not in token:
                continue

            key, val = token.split("=")
            if key in self.field_lists_map:
                # Multi-line assignment
                if val.startswith("(") and not val.endswith(")"):
                    val = [val, lexer.get_token()]
                    while val[-1] is not None and not val[-1].endswith(")"):
                        val.append(lexer.get_token())
                    val = list(map(lambda x: x.strip().lstrip("(").rstrip(",)"), val))
                elif "," in val: # Multi-item assignment on single line
                    val = val.lstrip("(").rstrip(")")
                    val = list(map(lambda x: x.strip(), val.split(",")))
                else: # Single item
                    val = [val.lstrip("(").rstrip(")")]
                val = list(filter(None, val))
            else:
                val = val.lstrip("(").rstrip(")")
            self._var_map[key] = val

