import unittest


class TestCase(unittest.TestCase):
    """Wrapper to unify Py3k's TestCase with Py2.x's."""
    def assertCountEqual(self, *args, **kwargs):
        """Define this method for Py2.x, delegate to parent on Py3k."""
        return (super(TestCase, self).assertCountEqual(*args, **kwargs)
                if hasattr(super(TestCase, self), "assertCountEqual") else
                super(TestCase, self).assertItemsEqual(*args, **kwargs))
