# -*- coding: utf-8 -*-
from . import TestCase
from ..aurpackage import AURPackage
from datetime import datetime
import random, string


class TestAURPackage(TestCase):
    def _randstr(self, length=16):
        return "".join([random.choice(string.ascii_letters) for _ in range(length)])

    def test_fuzzed_aur_returns(self):
        fuzzy_dict = dict([
            (self._randstr(random.choice([5, 8, 12])),
             self._randstr(random.choice([15, 20, 50])))
            for _ in range(12)
        ])
        pkg = AURPackage(fuzzy_dict)
        self.assertEqual(len([key for key in fuzzy_dict.keys() if key in pkg]), 0)
        [self.assertIsNone(getattr(pkg, key))
         for key, val in fuzzy_dict.iteritems()
         if key in pkg]

    def test_sane_aur_returns(self):
        target_dict = {
            "Name": "otf-texgyre",
            "Version": "2.005-2",
            "Description": "High quality open source opentype font collection resembling Helvetica, Times, Palatino, Courier, Gothic, Bookman, Schoolbook, and Chancery.",
            "URL": "http://www.gust.org.pl/projects/e-foundry/tex-gyre",
            "ID": 40619,
            "CategoryID": 20,
            "Maintainer": "lspci",
            "License": "custom:GUST Font License",
            "FirstSubmitted": 1283923419,
            "LastModified": 1379911535,
            "OutOfDate": 0,
            "NumVotes": 132,
            "URLPath": "https://aur.archlinux.org/packages/ot/otf-texgyre/otf-texgyre.tar.gz",
            "URLPkgbuild": "https://aur.archlinux.org/packages/ot/otf-texgyre/PKGBUILD",
        }
        pkg = AURPackage(target_dict)
        [self.assertEqual(val, getattr(pkg, key.lower()))
         for key, val in target_dict.iteritems()
         if key.lower() in pkg and key.lower() != "version"]

        self.assertEqual("2.005", pkg.version)
        self.assertEqual("2", pkg.release)
