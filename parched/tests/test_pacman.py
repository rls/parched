# -*- coding: utf-8 -*-
from __future__ import print_function
from . import TestCase
from ..pacman import Pacman
from datetime import datetime
from io import StringIO, TextIOBase
import tarfile
try:
    # Python 3.3 up
    from unittest.mock import ANY, MagicMock, Mock, patch
except ImportError:
    from mock import ANY, MagicMock, Mock, patch


class TestPacman(TestCase):
    tarfile_mock_contents = ["lorem", "ipsum", "dolor", "sit", "amet"]
    sane_ish_pkginfo = {
        "pkgname": "git",
        "pkgver": "1.8.3.4-1",
        "pkgdesc": "the fast distributed version control system",
        "url": "http://git-scm.com/",
        "builddate": 1374763139,
        "packager": "Dan McGee <dan@archlinux.org>",
        "size": 20495360,
        "arch": ["i686"],
        "license": ["GPL2"],
        "replaces": ["git-core"],
        "provides": ["git-core"],
        "depend": ["curl", "expat>=2.0", "perl-error", "perl>=5.14.0", "openssl", "pcre"],
        "optdepend": [
            "tk: gitk and git gui",
            "perl-libwww: git svn",
            "perl-term-readkey: git svn",
            "perl-mime-tools: git send-email",
            "perl-net-smtp-ssl: git send-email TLS support",
            "perl-authen-sasl: git send-email TLS support",
            "python2: various helper scripts",
            "subversion: git svn",
            "cvsps: git cvsimport",
            "gnome-keyring: GNOME keyring credential helper"
        ],
        "makedepend": ["python2", "emacs", "libgnome-keyring"],
        "makepkgopt": [
            "strip",
            "docs",
            "libtool",
            "staticlibs",
            "emptydirs",
            "zipman",
            "purge",
            "!upx",
            "!debug"
        ]
    }

    def setUp(self):
        self.pkginfo_mock = MagicMock(spec=TextIOBase)
        self.tarfile_mock = MagicMock(spec=tarfile.TarFile)
        self.tarfile_mock.getnames.return_value = self.tarfile_mock_contents
        self.tarfile_mock.extractfile.return_value = self.pkginfo_mock

        self.tarfile_open_mock = Mock(return_value=self.tarfile_mock)

    def test_init_with_file(self):
        """Create an instance of Pacman using a file-like object."""
        fd_mock = Mock()
        fd_mock.close.side_effect = Exception("shouldn't close()")

        with patch("tarfile.open", self.tarfile_open_mock):
            pkg_fd = Pacman(fd_mock)
            self.tarfile_open_mock.assert_called_once_with(
                fileobj=fd_mock, 
                mode=ANY
            )
            self.tarfile_mock.getnames.assert_called_once_with()
            self.tarfile_mock.extractfile.assert_called_once_with(".PKGINFO")
        self.assertListEqual(pkg_fd.files, self.tarfile_mock_contents)

    def test_init_with_path(self):
        """Create an instance of Pacman using a path to a file."""
        with patch("tarfile.open", self.tarfile_open_mock):
            bogus_path = "/tmp/mocks/are/big/boy/toys"
            pkg = Pacman(bogus_path)
            self.tarfile_open_mock.assert_called_once_with(
                bogus_path,
                mode=ANY
            )
            self.tarfile_mock.getnames.assert_called_once_with()
            self.tarfile_mock.extractfile.assert_called_once_with(".PKGINFO")
        self.assertListEqual(pkg.files, self.tarfile_mock_contents)

    def test_parsing_sane_pkginfo(self):
        """Check that sane-ish .PKGINFOs are parsed correctly."""
        # Build PKGINFO
        pkginfo = StringIO()
        for key, item in self.sane_ish_pkginfo.items():
            try:
                if isinstance(item, str):
                    raise TypeError
                for val in item:
                    print(u"{0} = {1}".format(key, val), file=pkginfo)
            except TypeError:
                print(u"{0} = {1}".format(key, item), file=pkginfo)

        self.tarfile_mock.extractfile.return_value = pkginfo
        fd_mock = Mock()
        fd_mock.close.side_effect = Exception("should't close()")

        with patch("tarfile.open", self.tarfile_open_mock):
            pkg_fd = Pacman(fd_mock)
            for key, item in self.sane_ish_pkginfo.items():
                attr_name = pkg_fd.field_to_attr(key)
                attr_val = getattr(pkg_fd, attr_name)
                if attr_name in pkg_fd.field_lists_map:
                    self.assertCountEqual(attr_val, item)
                elif isinstance(attr_val, datetime):
                    self.assertEqual(attr_val,
                                     datetime.utcfromtimestamp(int(item)))
                elif key == "pkgver":
                    version, _, release = item.rpartition("-")
                    self.assertEqual(attr_val, version)
                else:
                    self.assertEqual(attr_val, item)
