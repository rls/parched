# -*- coding: utf-8 -*-
from . import TestCase
from ..pkgbuild import Pkgbuild, Checksum
from io import BytesIO, TextIOWrapper
import hashlib, pkgutil, unittest
try:
    # Python 3.3 up
    from unittest.mock import ANY, MagicMock, Mock, mock_open, patch
except ImportError:
    from mock import ANY, MagicMock, Mock, mock_open, patch


class TestPkgbuild(TestCase):
    def test_init_with_path(self):
        """Create an instance of Pkgbuild using a path to a file."""
        mocked_open = mock_open()
        with patch("parched.pkgbuild.open", mocked_open, create=True):
            bogus_path = "/tmp/mocks/are/big/boy/toys"
            pkgbuild = Pkgbuild(bogus_path)
            mocked_open.assert_called_once_with(bogus_path, mode=ANY)

    def test_parsing_straighforward_pkgbuild(self):
        """Check that sane-ish PKGBUILDs are parsed correctly.

        Also doubles as a test for instantiating a Pkgbuild using a
        file-like object.  (Efficient, no?)

        This is kind of weak-sauce, as I’d rather generate a PKGBUILD
        from known data, so it’d be easier to check assertions.
        However, that involves writing a PKGBUILD *builder*, which I’m
        not terribly inclined to tackle at the moment.
        """
        try:
            pkg = Pkgbuild(BytesIO(
                pkgutil.get_data(__package__, "straightforward.PKGBUILD")
            ))
        except TypeError:
            pkg = Pkgbuild(TextIOWrapper(BytesIO(
                pkgutil.get_data(__package__, "straightforward.PKGBUILD")
            ), encoding="utf8"))

        self.assertEqual("parched-pkgbuild-sample", pkg.name)
        self.assertEqual("1.0", pkg.version)
        self.assertEqual("1", pkg.release)
        self.assertEqual(["any"], pkg.architectures)
        self.assertEqual(["GPL"], pkg.licenses)

        assert_depends = [["python2"], pkg.depends]
        self.assertCountEqual(*assert_depends)

        self.assertGreater(len(pkg.description), 0)
        self.assertEqual("A relatively simple, sample PKGBUILD",
                         pkg.description)

        self.assertGreater(len(pkg.url), 0)
        self.assertEqual("http://www.archlinux.org/", pkg.url)

        self.assertGreater(len(pkg.sources), 0)
        assert_sources = [
            ["https://www.example.org/{0}_{1}.tar.gz".format(
                pkg.name,
                pkg.version
            )],
            pkg.sources
        ]
        self.assertCountEqual(*assert_sources)

        [self.assertCountEqual(
            [Checksum(checksum=hashlib.md5(url.encode('utf-8')).hexdigest(),
                      algo="md5")],
            pkg.checksums[url])
        for url in pkg.sources]

    def test_parsing_edgecases(self):
        """Check recipe constructs that define the limit of the parser.

        By no means is this an *true* test of how edge cases are
        handled, merely as accurate a representation of the
        idiosyncratic limits Pkgbuild (in its current form) will
        tolerate as I can make it.

        A lot of this will likely be just idiomatic bash---but, as noted
        in Pkgbuild’s docstring, Pkgbuild was, and will, never be a
        complete bash parser.
        """
        try:
            pkg = Pkgbuild(BytesIO(
                pkgutil.get_data(__package__, "edgecases.PKGBUILD")
            ))
        except TypeError:
            pkg = Pkgbuild(TextIOWrapper(BytesIO(
                pkgutil.get_data(__package__, "edgecases.PKGBUILD")
            ), encoding="utf8"))

        self.assertEqual("parched-pkgbuild-sample-edge", pkg.name)
        self.assertEqual("1.0", pkg.version)
        self.assertEqual("1", pkg.release)
        self.assertEqual(["any"], pkg.architectures)
        self.assertEqual(["GPL"], pkg.licenses)

        assert_depends = [["python2", "python"], pkg.depends]
        self.assertCountEqual(*assert_depends)

        self.assertGreater(len(pkg.description), 0)
        self.assertEqual("A more complicated (though still valid) PKGBUILD",
                         pkg.description)

        self.assertGreater(len(pkg.url), 0)
        self.assertEqual("http://www.archlinux.org/", pkg.url)

        self.assertGreater(len(pkg.sources), 0)
        assert_sources = [
            ["https://www.example.org/{0}_{1}.tar.gz".format(
                pkg.name,
                pkg.version
            ),
            "https://www.example.org/parched-fictional-patch.diff.gz"],
            pkg.sources
        ]
        self.assertCountEqual(*assert_sources)

        for url in pkg.sources:
            target_checksums = []
            for algo in ["md5", "sha1", "sha256", "sha512"]:
                if algo == "sha512" and len(pkg.checksums[url]) < 4:
                    continue
                algo_func = getattr(hashlib, algo)
                target_checksums.append(
                    Checksum(checksum=algo_func(url.encode("utf-8")).hexdigest(),
                             algo=algo)
                )
            self.assertCountEqual(target_checksums, pkg.checksums[url])
