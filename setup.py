from setuptools import setup, find_packages
from setuptools.command.test import test
import os.path, sys, unittest
import parched

class DiscoverTests(test):
    def finalize_options(self):
        test.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        setup_file = sys.modules["__main__"].__file__
        setup_dir = os.path.abspath(os.path.dirname(setup_file))

        test_runner = unittest.TextTestRunner(verbosity=2)
        test_suite = unittest.defaultTestLoader.discover(setup_dir)
        test_runner.run(test_suite)

config = {
    "name": "parched",
    "version": parched.__version__,
    "description": parched.__description__,
    "author": "Neil Santos",
    "author_email": "python@neil.diosa.ph",
    "url": "http://rls.bitbucket.org/parched",
    "packages": find_packages(),
    "package_data": {"tests": ["*.PKGBUILD"]},
    "license": "MIT",
    "classifiers": [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Interpreters',
    ],
    "cmdclass": {"test": DiscoverTests}
}

if sys.version_info.major == 2:
    # Py3k has this as unittest.mock
    config["extras_require"] = {"test": ["mock >= 1.0.1"]}

setup(**config)
